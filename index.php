<?php
/**
 * Date: 31.03.2016
 * @author arkataev
 * https://github.com/arkataev
 */



?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="resources/css/bootstrap.min.css">
	<title>Title</title>
</head>
<body>

<form name="filter" id="filter" action="src/controller/filter.php" method="POST">
	<div class="radio form-group" id="sex">
		<label>
			<input type="radio" name="sex" value="male">
			Мужской <span class="count"></span>
		</label>
		<label>
			<input type="radio" name="sex" value="female">
			Женский <span class="count"></span>
		</label>
	</div>
	<div class="radio form-group" id="age">
		<label>
			<input type="radio" name="age" value="16-20">
			16-20 <span class="count"></span>
		</label>
		<label>
			<input type="radio" name="age" value="20-30">
			20-30 <span class="count"></span>
		</label>
		<label>
			<input type="radio" name="age" value="30-50">
			30-50 <span class="count"></span>
		</label>
	</div>
	<div class="radio form-group" id="education">
		<label>
			<input type="radio" name="education"  value="middle">
			Среднее <span class="count"></span>
		</label>
		<label>
			<input type="radio" name="education"  value="special">
			Среднее специальное <span class="count"></span>
		</label>
		<label>
			<input type="radio" name="education"  value="high">
			Высшее <span class="count"></span>
		</label>
	</div>
</form>

<div id="result"></div>

</body>
</html>


<script type="text/javascript">

	window.onload = function () {
		var load = ajax(new FormData(document.getElementById("filter")));
		console.log(load);
	}

	var buttonsArray = Array.prototype.slice.call(document.querySelectorAll('[type = "radio"]'));
	var inputs = Array.prototype.slice.call(document.querySelectorAll('input'));
	var counts = Array.prototype.slice.call(document.getElementsByClassName('count'));
	
	buttonsArray.forEach(function(item){
		item.addEventListener('change', function () {
			console.log (item.value);
			ajax(new FormData(document.getElementById("filter")));
		})
	})

	var parseResult = function (result, inputs) {
		var counter = [];
		inputs.map(function (input) {
			counter.push(result.filter(function (r_item) {
				var age = input.value.split('-');
				if (r_item.sex == input.value || r_item.education == input.value || age[0] <= r_item.age && age[1] > r_item.age) {
					return true;
				}
			}))
		})

		console.log(counter);
	}


	// Ajax -request
	var ajax = function (data) {
		var xhr = new XMLHttpRequest();
		xhr.open('POST', 'src/controller/filter.php', true);
		xhr.send(data);

		xhr.onreadystatechange = function () {
			if(xhr.readyState == 4 && xhr.status == 200) {
				parseResult(JSON.parse(this.responseText), inputs);
			}
		}
	}

</script>