<?php
/**
 * Date: 31.03.2016
 * @author arkataev
 * https://github.com/arkataev
 */

namespace Model;

use PDO;

class Model
{
	protected $db;
	private $config = array(
		'dsn' 		=> "mysql",
		'host'		=> "localhost",
		'db_name'	=> "advanced_online",
		'user' 		=> "root",
		'password' 	=> "78nbtchf",
		'options'	=> ""
	);
	
	function __construct()
	{
		try {
			$this->db = new PDO( $this->config[ 'dsn' ] . ":dbname=" . $this->config[ 'db_name' ] . ';' . 'host =' . $this->config[ 'host' ], $this->config[ 'user' ], $this->config[ 'password' ] );
		} catch ( Exception $e ) {
			echo 'Could not connect to DataBase. Error: ' . $e->getMessage();
		}
	}
}