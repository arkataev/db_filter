<?php
/**
 * Date: 31.03.2016
 * @author arkataev
 * https://github.com/arkataev
 */

namespace Model;

use PDO;

class UserModel extends Model
{

	public function getUserFilteredData( array $query )
	{
		$stmt = $this->db->prepare($query['query'], array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

		if($query['data']['sex'] != NULL) { $params[':sex'] = $query['data']['sex']; }
		if($query['data']['age'] != NULL) { $params[':age_min'] = $query['data']['age']['min']; $params[':age_max'] = $query['data']['age']['max']; }
		if($query['data']['education'] != NULL) { $params[':education'] = $query['data']['education']; }

		$stmt->execute($params);

		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
}