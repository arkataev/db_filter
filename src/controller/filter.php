<?php
/**
 * Date: 31.03.2016
 * @author arkataev
 * https://github.com/arkataev
 */

namespace Controller;
require_once '../../vendor/autoload.php';

use Core\FilterController as Filter;
use Model\UserModel;
use Exception;

try {
	
	$user = new UserModel();
	$query = Filter::getFilterQuery();
	$user_data = $user->getUserFilteredData( $query );

	exit(json_encode($user_data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
	
}catch (Exception $e){
	
	echo $e->getMessage();
	
}

