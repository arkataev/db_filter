<?php
/**
 * Date: 31.03.2016
 * @author arkataev
 * https://github.com/arkataev
 */

namespace Core;

class FilterController
{
	/**
	 * @return string
	 * Принимает данные для запроса формирует и возвращает запрос для отправки в БД
	 * на основе, которого будет производится фильтрация
	 */
	public static function getFilterQuery() {

		$filter_data = array(
			"sex" => $_POST['sex'] ? htmlspecialchars( $_POST['sex'] ) : NULL,
			"age" => $_POST['age'] ? array(
				"min" => htmlspecialchars( explode('-', $_POST['age'])[0] ),
				"max" => htmlspecialchars( explode('-', $_POST['age'])[1] )
			) : [],
			"education" => $_POST ? htmlspecialchars( $_POST['education'] ) : NULL
		);

		$query = 'SELECT * FROM ';
		extract($filter_data, EXTR_OVERWRITE);

		if( $age != [] ) {
			$query .= '(SELECT * FROM users WHERE age >= :age_min AND age < :age_max) as users ';
		}else {
			$query .= ' users ';
		}

		if( $sex AND $education) {
			$query .= 'WHERE sex= :sex AND education= :education';
		}elseif( $sex || $education ) {
			$value = $sex ? $sex : $education;
			$key = array_search($value, $filter_data);
			$query .= 'WHERE ' . $key . '= :' . $key;
		}

		return array('query' => $query, 'data' => $filter_data);
	}
		
}